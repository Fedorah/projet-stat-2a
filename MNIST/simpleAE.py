import torch
import torchvision
from torch.autograd import Variable
import torch.nn as nn
import torch.utils.data as Data
import torch.nn.functional as F
import matplotlib.pyplot as plt
import numpy as np
import time
import progressbar
import Models as models
import math


class SimpleAE(nn.Module):
    
    def __init__(self):
        super(SimpleAE, self).__init__()
        self.encoder = nn.Sequential(
            nn.Conv2d(1, 64, 7),
            nn.ReLU(),
            nn.Conv2d(64, 32, 3, stride=2, padding=1),
            nn.ReLU(),
            nn.Conv2d(32, 16, 3, stride=2, padding=1)
        )
        
        self.decoder = nn.Sequential(
            nn.ConvTranspose2d(16, 32, 3, stride=2, padding=1),
            nn.ReLU(),
            nn.ConvTranspose2d(32, 64, 3, stride=2, padding=1, output_padding=1),
            nn.ReLU(),
            nn.ConvTranspose2d(64, 1, 7),
            torch.nn.Sigmoid(),
        )
        
    def forward(self, x):
        x = self.encoder(x)
        x = self.decoder(x)
        return x

def train_model_2(param):
    #  use gpu if available
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # create a model from `AE` autoencoder class
    # load it to gpu
    model = SimpleAE()
    model = model.cuda()
    # create an optimizer object
    # SGD optimizer
    optimizer_ae = torch.optim.SGD(model.parameters(), lr=param["LR"], weight_decay=param["WD"])

    # mean-squared error loss
    criterion = nn.MSELoss()

    train_dataset = torchvision.datasets.MNIST(
        root="../data", 
        train=True, 
        transform=torchvision.transforms.ToTensor(), 
        download=False
    )

    test_dataset = torchvision.datasets.MNIST(
        root="../data", 
        train=False, 
        transform=torchvision.transforms.ToTensor(), 
        download=False
    )

    train_loader = torch.utils.data.DataLoader(
        train_dataset, batch_size=128, shuffle=True
    )

    test_loader = torch.utils.data.DataLoader(
        test_dataset, batch_size=128, shuffle=True
    )
    
    model.train()
    
    for epoch in range(param["epoch"]):
        pred = 0
        correct = 0
        loss = 0
        for batch_features, _ in train_loader:
            
            # reset the gradients back to zero
            # PyTorch accumulates gradients on subsequent backward passes
            optimizer_ae.zero_grad()

            # compute reconstructions
            outputs = model(batch_features.cuda()) # fetch output image
            # outputs = Variable(outputs.cuda(), requires_grad=True) # transform output to variable so we can use requires_grad 
            
            # compute training reconstruction loss
            # batch_features = Variable(batch_features.cuda(), requires_grad=True)
            train_loss = criterion(outputs, batch_features.cuda())
            
            # compute accumulated gradients
            train_loss.backward() 

            # perform parameter update based on current gradients
            optimizer_ae.step()

            # add the mini-batch training loss to epoch loss
            loss += train_loss
            
            # pred = outputs.data.max(1, keepdim=True)[1]
            # correct += pred.eq(b_y.data.view_as(pred)).long().cpu().sum()
        print("epoch : {}/{}, loss = {:.6f}".format(epoch + 1, param["epoch"], loss / len(train_loader)))
        
    # compute the epoch training loss
    loss = loss / len(train_loader)

    # display the epoch training loss
    print("epoch : {}/{}, loss = {:.6f}".format(epoch + 1, param["epoch"], loss))
     
    # testing
    model.eval()
    reconstruction = []
    test_examples = None
    with torch.no_grad():
        for batch_features, _ in test_loader:
            reconstruction.append(model(batch_features.cuda()))
            test_examples = batch_features
            break
        
    # visualization
    plt.figure(figsize=(20, 4))
    with torch.no_grad():
        for i in range(10):
            # display original
            ax = plt.subplot(2, 10, i + 1)
            plt.imshow(test_examples[i][0].numpy().reshape(28, 28, 1))
            plt.gray()
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)

            # display reconstruction
            ax = plt.subplot(2, 10, i + 1 + 10)
            plt.imshow(reconstruction[0][i].cpu().numpy().reshape(28, 28, 1))
            plt.gray()
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)
        plt.savefig("AE_MNIST_res" + str(math.floor(time.time())), bbox_inches='tight')
        
    # Loss_test[epoch] = test_loss/len(test_loader.dataset)
    # Acc_test[epoch] =  100 *  float(correct) /float(len(test_loader.dataset))

            
if __name__ == "__main__":
    param = {"m1": 128, "m2": 32, "m3": 128, "m4": 8, "m5": 32, "m6": 64, "m7": 128,"LR": 0.01, "WD": 1e-5, "MN": 0.9, "gamma_sched": 0.2, "step_size_sch": 30, "Unfoldings": 6, "BS": 128, "epoch": 50, "mnist": False}
    train_model_2(param)
