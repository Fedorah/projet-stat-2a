import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.utils.data as Data
import torch.nn.functional as F
import torchvision
import matplotlib.pyplot as plt
import numpy as np
import time
import math
import progressbar
import Models as models


class ML_ISTA_NET_AE(nn.Module):
    def __init__(self, m1, m2, m3, m4, m5, m6, T):
        super(ML_ISTA_NET_AE, self).__init__()
        
        self.m1 = m1 # 128
        self.m2 = m2 # 64
        self.m3 = m3 # 32
        self.m4 = m4 # 8
        self.m5 = m5 # 32
        self.m6 = m6 # 64
        self.T = T
        
        # Convolutional Filters encoder
        self.W1 = nn.Parameter(torch.randn(64, 1, 7, 7), requires_grad=True)
        self.strd1 = 1;
        self.W2 = nn.Parameter(torch.randn(32, 64, 3, 3), requires_grad=True)
        self.strd2 = 2;
        self.W3 = nn.Parameter(torch.randn(16, 32, 3, 3), requires_grad=True)
        self.strd3 = 2;
        
        self.W4 = nn.Parameter(torch.randn(16, 32, 3, 3), requires_grad=True)
        self.strd4 = 2;
        self.W5 = nn.Parameter(torch.randn(32, 64, 3, 3), requires_grad=True)
        self.strd5 = 2;
        self.W6 = nn.Parameter(torch.randn(64, 1, 7, 7), requires_grad=True)
        self.strd6 = 1;
        
        self.c1 = nn.Parameter(torch.ones(1,1,1,1), requires_grad=True)
        self.c2 = nn.Parameter(torch.ones(1,1,1,1), requires_grad=True)
        self.c3 = nn.Parameter(torch.ones(1,1,1,1), requires_grad=True)
        
        self.c4 = nn.Parameter(torch.ones(1,1,1,1), requires_grad=True)
        self.c5 = nn.Parameter(torch.ones(1,1,1,1), requires_grad=True)
        self.c6 = nn.Parameter(torch.ones(1,1,1,1), requires_grad=True)
        
        # Biases / Thresholds
        self.b1 = nn.Parameter(torch.zeros(1,64,1,1), requires_grad=True)
        self.b2 = nn.Parameter(torch.zeros(1,32,1,1), requires_grad=True)
        self.b3 = nn.Parameter(torch.zeros(1,16,1,1), requires_grad=True)
        
        self.b4 = nn.Parameter(torch.zeros(1,16,1,1), requires_grad=True)
        self.b5 = nn.Parameter(torch.zeros(1,32,1,1), requires_grad=True)
        self.b6 = nn.Parameter(torch.zeros(1,64,1,1), requires_grad=True)
        
        # Classifier
        self.Wclass = nn.Linear(m3, 10)
        
        # Initialization
        self.W1.data = .1 /np.sqrt(49) * self.W1.data
        self.W2.data = .1 /np.sqrt(m1*36) * self.W2.data
        self.W3.data = .1 /np.sqrt(m2*16) * self.W3.data
        self.W4.data = .1 /np.sqrt(m3*16) * self.W4.data
        self.W5.data = .1 /np.sqrt(m4*4) * self.W5.data
        self.W6.data = .1 /np.sqrt(m5*4) * self.W6.data

        
        
    def forward(self, x):
        # Encoding
        # x 128, 1, 28, 28
        gamma1 = F.relu(self.c1*F.conv2d(x, self.W1, stride=self.strd1) + self.b1) #128, 128, 12, 12
        #print(gamma1.shape)
        gamma2 = F.relu(self.c2*F.conv2d(gamma1, self.W2, stride=self.strd2, padding=1) + self.b2) 
        #print(gamma2.shape)
        gamma3 = F.relu(self.c3*F.conv2d(gamma2, self.W3, stride=self.strd3, padding=1) + self.b3)
        #print(gamma3.shape)
        gamma4 = F.relu(self.c4*F.conv2d(gamma3, self.W4, stride=self.strd4, padding=1) + self.b4)
        #print(gamma4.shape)
        gamma5 = F.relu(self.c5*F.conv2d(gamma4, self.W5, stride=self.strd5, padding=1) + self.b5)
        #print(gamma5.shape)
        gamma6 = F.relu(self.c6*F.conv2d(gamma5, self.W6, stride=self.strd6) + self.b6)
        #print(gamma6.shape)

        
        for _ in  range(self.T):
            
            # backward computation
            gamma5 = F.conv_transpose2d(gamma6,self.W6, stride=self.strd6)
            gamma4 = F.conv_transpose2d(gamma5,self.W5, stride=self.strd5, padding=1)
            gamma3 = F.conv_transpose2d(gamma4,self.W4, stride=self.strd4, padding=1)
            gamma2 = F.conv_transpose2d(gamma3,self.W3, stride=self.strd3, padding=1, output_padding=1)
            gamma1 = F.conv_transpose2d(gamma2,self.W2, stride=self.strd2, padding=1)

            # forward computation
            gamma1 = F.relu( (gamma1 - self.c1*F.conv2d( F.conv_transpose2d(gamma1,self.W1, stride = self.strd1) - x ,self.W1, stride = self.strd1)) + self.b1) # 128, 128, 12, 12
            gamma2 = F.relu( (gamma2 - self.c2*F.conv2d( F.conv_transpose2d(gamma2,self.W2, stride = self.strd2) - gamma1, self.W2, stride = self.strd2)) + self.b2) # 128, 32, 4, 4
            gamma3 = F.relu( (gamma3 - self.c3*F.conv2d( F.conv_transpose2d(gamma3,self.W3, stride = self.strd3) - gamma2, self.W3, stride = self.strd3)) + self.b3) # 128, 8, 1, 1
            gamma4 = F.relu( (gamma4 - self.c4*F.conv2d( F.conv_transpose2d(gamma4,self.W4, stride = self.strd4) - gamma3, self.W4, stride = self.strd4)) + self.b4)
            # print(gamma3.shape)
            # print(F.conv2d(gamma5 ,self.W5bis, stride = self.strd5).shape)
            gamma5 = F.conv2d(gamma5 ,self.W5bis, stride = self.strd5)
            # print(gamma5.shape)
            # print(gamma4.shape)
            gamma5 -= gamma4
            gamma5 = F.conv_transpose2d(gamma5)
            gamma5 = F.relu( (gamma5 - self.c5*F.conv_transpose2d( F.conv2d(gamma5 ,self.W5bis, stride = self.strd5) - gamma4, self.W5bis, stride = self.strd5)) + self.b5)
            gamma6 = F.relu( (gamma6 - self.c6*F.conv_transpose2d( F.conv2d(gamma6 ,self.W6, stride = self.strd6) - gamma5, self.W6, stride = self.strd6)) + self.b6)
            
            """print("\n")
            print(gamma1.shape)
            print(gamma2.shape)
            print(gamma3.shape)
            print(gamma4.shape)
            print(gamma5.shape)
            print(gamma6.shape)"""

            
        return gamma6
        

def train_model_2(param):
    #  use gpu if available
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # create a model from `AE` autoencoder class
    # load it to gpu
    model = ML_ISTA_NET_AE(param["m1"], param["m2"], param["m3"], param["m4"], param["m5"], param["m6"], param["Unfoldings"])
    model = model.cuda()
    # create an optimizer object
    # SGD optimizer
    optimizer_ae = torch.optim.SGD(model.parameters(), lr=param["LR"], weight_decay=param["WD"])

    # mean-squared error loss
    criterion = nn.MSELoss()

    train_dataset = torchvision.datasets.MNIST(
        root="../data", 
        train=True, 
        transform=torchvision.transforms.ToTensor(), 
        download=False
    )

    test_dataset = torchvision.datasets.MNIST(
        root="../data", 
        train=False, 
        transform=torchvision.transforms.ToTensor(), 
        download=False
    )

    train_loader = torch.utils.data.DataLoader(
        train_dataset, batch_size=128, shuffle=True
    )

    test_loader = torch.utils.data.DataLoader(
        test_dataset, batch_size=128, shuffle=True
    )
    
    model.train()
    
    for epoch in range(param["epoch"]):
        pred = 0
        correct = 0
        loss = 0
        for batch_features, _ in train_loader:
            
            # reset the gradients back to zero
            # PyTorch accumulates gradients on subsequent backward passes
            optimizer_ae.zero_grad()

            # compute reconstructions
            outputs = model(batch_features.cuda()) # fetch output image
            # outputs = Variable(outputs.cuda(), requires_grad=True) # transform output to variable so we can use requires_grad 
            
            # compute training reconstruction loss
            # batch_features = Variable(batch_features.cuda(), requires_grad=True)
            train_loss = criterion(outputs, batch_features.cuda())
            
            # compute accumulated gradients
            train_loss.backward() 

            # perform parameter update based on current gradients
            optimizer_ae.step()

            # add the mini-batch training loss to epoch loss
            loss += train_loss.item()
        print("epoch : {}/{}, loss = {:.6f}".format(epoch + 1, param["epoch"], loss / len(train_loader)))
        
    # compute the epoch training loss
    loss = loss / len(train_loader)

    # display the epoch training loss
    print("epoch : {}/{}, loss = {:.6f}".format(epoch + 1, param["epoch"], loss))
    
    
    
    
    # testing
    model.eval()
    reconstruction = []
    test_examples = None
    with torch.no_grad():
        for batch_features, _ in test_loader:
            reconstruction.append(model(batch_features.cuda()))
            test_examples = batch_features
            break
        
    # visualization
    plt.figure(figsize=(20, 4))
    with torch.no_grad():
        for i in range(10):
            # display original
            ax = plt.subplot(2, 10, i + 1)
            plt.imshow(test_examples[i][0].numpy().reshape(28, 28, 1))
            plt.gray()
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)

            # display reconstruction
            ax = plt.subplot(2, 10, i + 1 + 10)
            plt.imshow(reconstruction[0][i].cpu().numpy().reshape(28, 28, 1))
            plt.gray()
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)
        plt.savefig("AE_MNIST_res" + str(math.floor(time.time())), bbox_inches='tight')
        
    # Loss_test[epoch] = test_loss/len(test_loader.dataset)
    # Acc_test[epoch] =  100 *  float(correct) /float(len(test_loader.dataset))


if __name__ == "__main__":
    param = {"m1": 128, "m2": 64, "m3": 32, "m4": 8, "m5": 32, "m6": 64, "m7": 128,"LR": 0.01, "WD": 1e-5, "MN": 0.9, "gamma_sched": 0.2, "step_size_sch": 30, "Unfoldings": 6, "BS": 128, "epoch": 15, "mnist": False}
    train_model_2(param)
