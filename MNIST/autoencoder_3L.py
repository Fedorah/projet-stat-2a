import torch
import torchvision
from torch.autograd import Variable
import torch.nn as nn
import torch.utils.data as Data
import torch.nn.functional as F
import matplotlib.pyplot as plt
import numpy as np
import time
import progressbar
import Models as models
import math


class AE_3L(nn.Module):
    def __init__(self):
        super(AE_3L, self).__init__()
        # Convolutional Filters encoder
        self.W1 = nn.Parameter(torch.randn(64, 1, 6, 6), requires_grad=True)
        self.strd1 = 2;
        self.W2 = nn.Parameter(torch.randn(32, 64, 6, 6), requires_grad=True)
        self.strd2 = 2;
        self.W3 = nn.Parameter(torch.randn(16, 32, 4, 4), requires_grad=True)
        self.strd3 = 1;
        self.W4 = nn.Parameter(torch.randn(32, 16, 1, 1), requires_grad=True)
        self.strd4 = 1;
        self.W5 = nn.Parameter(torch.randn(64, 32, 6, 6), requires_grad=True)
        self.strd5 = 1;
        self.W6 = nn.Parameter(torch.randn(16, 1, 6, 6), requires_grad=True)
        self.strd6 = 1;
        self.W = nn.Parameter(torch.randn(1, 64, 6, 6), requires_grad=True)
        
        self.c1 = nn.Parameter(torch.ones(1, 1, 1, 1), requires_grad=True)
        self.c2 = nn.Parameter(torch.ones(1, 1, 1, 1), requires_grad=True)
        self.c3 = nn.Parameter(torch.ones(1, 1, 1, 1), requires_grad=True)
        
        # Biases / Thresholds
        self.b1 = nn.Parameter(torch.zeros(1, 64, 1, 1), requires_grad=True)
        self.b2 = nn.Parameter(torch.zeros(1, 32, 1, 1), requires_grad=True)
        self.b3 = nn.Parameter(torch.zeros(1, 16, 1, 1), requires_grad=True)
        
        # Initialization
        self.W1.data = .1 /np.sqrt(64*36) * self.W1.data
        self.W2.data = .1 /np.sqrt(32*16) * self.W2.data
        self.W3.data = .1 /np.sqrt(16*16) * self.W3.data
        
    def forward(self, x):
        # Encoding
        gamma1 = F.relu(self.c1*F.conv2d(x, self.W1, stride=self.strd1) + self.b1)
        gamma2 = F.relu(self.c2*F.conv2d(gamma1, self.W2, stride=self.strd2) + self.b2) 
        gamma3 = F.relu(self.c3*F.conv2d(gamma2, self.W3, stride=self.strd3) + self.b3) 
        
        for _ in  range(6):
            
            # backward computation
            gamma2 = F.conv_transpose2d(gamma3,self.W3, stride=self.strd3)
            gamma1 = F.conv_transpose2d(gamma2,self.W2, stride=self.strd2)
            
            # forward computation
            gamma1 = F.relu((gamma1 - self.c1*F.conv2d(F.conv_transpose2d(gamma1, self.W1, stride=self.strd1) - x , self.W1, stride=self.strd1)) + self.b1)
            gamma2 = F.relu((gamma2 - self.c2*F.conv2d(F.conv_transpose2d(gamma2, self.W2, stride=self.strd2) - gamma1, self.W2, stride=self.strd2)) + self.b2) 
            gamma3 = F.relu((gamma3 - self.c3*F.conv2d(F.conv_transpose2d(gamma3, self.W3, stride=self.strd3) - gamma2, self.W3, stride=self.strd3)) + self.b3)
            
        # reverse
        gamma2 = F.relu((gamma3 - self.c2*F.conv_transpose2d(F.conv2d(gamma3, self.W4, stride=1) - gamma2, self.W4, stride=2) + self.b3))
        """print((F.conv2d(gamma2, self.W4, stride=2).shape))
        print(gamma1.shape)
        print((F.conv2d(gamma2, self.W2, stride=1) - gamma1).shape)
        gamma1 = F.relu((gamma2 - self.c1*F.conv_transpose2d(F.conv2d(gamma2, self.W1, stride=self.strd2) - gamma1, self.W2, stride=2)) + self.b1)"""
        gamma1 = F.conv_transpose2d(gamma2, self.W6)
        
            
        return gamma1

def train_model_2(param):
    #  use gpu if available
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # create a model from `AE` autoencoder class
    # load it to gpu
    model = AE_3L()
    model = model.cuda()
    # create an optimizer object
    # SGD optimizer
    optimizer_ae = torch.optim.SGD(model.parameters(), lr=param["LR"], weight_decay=param["WD"])

    # mean-squared error loss
    criterion = nn.MSELoss()

    train_dataset = torchvision.datasets.MNIST(
        root="../data", 
        train=True, 
        transform=torchvision.transforms.ToTensor(), 
        download=False
    )

    test_dataset = torchvision.datasets.MNIST(
        root="../data", 
        train=False, 
        transform=torchvision.transforms.ToTensor(), 
        download=False
    )

    train_loader = torch.utils.data.DataLoader(
        train_dataset, batch_size=128, shuffle=True
    )

    test_loader = torch.utils.data.DataLoader(
        test_dataset, batch_size=128, shuffle=True
    )
    
    model.train()
    
    for epoch in range(param["epoch"]):
        pred = 0
        correct = 0
        loss = 0
        for batch_features, _ in train_loader:
            
            # reset the gradients back to zero
            # PyTorch accumulates gradients on subsequent backward passes
            optimizer_ae.zero_grad()

            # compute reconstructions
            outputs = model(batch_features.cuda()) # fetch output image
            # outputs = Variable(outputs.cuda(), requires_grad=True) # transform output to variable so we can use requires_grad 
            
            # compute training reconstruction loss
            # batch_features = Variable(batch_features.cuda(), requires_grad=True)
            train_loss = criterion(outputs, batch_features.cuda())
            
            # compute accumulated gradients
            train_loss.backward() 

            # perform parameter update based on current gradients
            optimizer_ae.step()

            # add the mini-batch training loss to epoch loss
            loss += train_loss
        print("epoch : {}/{}, loss = {:.6f}".format(epoch + 1, param["epoch"], loss / len(train_loader)))
        
    # compute the epoch training loss
    loss = loss / len(train_loader)

    # display the epoch training loss
    print("epoch : {}/{}, loss = {:.6f}".format(epoch + 1, param["epoch"], loss))
     
    # testing
    model.eval()
    reconstruction = []
    test_examples = None
    with torch.no_grad():
        for batch_features, _ in test_loader:
            reconstruction.append(model(batch_features.cuda()))
            test_examples = batch_features
            break
        
    # visualization
    plt.figure(figsize=(20, 4))
    with torch.no_grad():
        for i in range(10):
            # display original
            ax = plt.subplot(2, 10, i + 1)
            plt.imshow(test_examples[i][0].numpy().reshape(28, 28, 1))
            plt.gray()
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)

            # display reconstruction
            ax = plt.subplot(2, 10, i + 1 + 10)
            plt.imshow(reconstruction[0][i].cpu().numpy().reshape(28, 28, 1))
            plt.gray()
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)
        plt.savefig("AE_MNIST_res" + str(math.floor(time.time())), bbox_inches='tight')
        
if __name__ == "__main__":
    param = {"m1": 128, "m2": 32, "m3": 128, "m4": 8, "m5": 32, "m6": 64, "m7": 128,"LR": 0.01, "WD": 1e-5, "MN": 0.9, "gamma_sched": 0.2, "step_size_sch": 30, "Unfoldings": 6, "BS": 128, "epoch": 50, "mnist": False}
    train_model_2(param)
