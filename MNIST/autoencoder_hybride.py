import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.utils.data as Data
import torch.nn.functional as F
import torchvision
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
import math
import time
import pdb


class ML_ISTA_NET_HAE(nn.Module):
    def __init__(self,m1,m2,m3,T):
        super(ML_ISTA_NET_HAE, self).__init__()
        
        self.T = T
        
        # Convolutional Filters
        self.W1 = nn.Parameter(torch.randn(m1,1,6,6), requires_grad=True)
        self.strd1 = 2;
        self.W2 = nn.Parameter(torch.randn(m2,m1,6,6), requires_grad=True)
        self.strd2 = 2;
        self.W3 = nn.Parameter(torch.randn(m3,m2,4,4), requires_grad=True)
        self.strd3 = 1;
        
        self.c1 = nn.Parameter(torch.ones(1,1,1,1), requires_grad=True)
        self.c2 = nn.Parameter(torch.ones(1,1,1,1), requires_grad=True)
        self.c3 = nn.Parameter(torch.ones(1,1,1,1), requires_grad=True)
        
        # Biases / Thresholds
        self.b1 = nn.Parameter(torch.zeros(1,m1,1,1), requires_grad=True)
        self.b2 = nn.Parameter(torch.zeros(1,m2,1,1), requires_grad=True)
        self.b3 = nn.Parameter(torch.zeros(1,m3,1,1), requires_grad=True)
        
        # Initialization
        self.W1.data = .1 /np.sqrt(36) * self.W1.data
        self.W2.data = .1 /np.sqrt(m1*36) * self.W2.data
        self.W3.data = .1 /np.sqrt(m2*16) * self.W3.data
        
    def forward(self, x):
        # Encoding
        gamma1 = F.relu(self.c1*F.conv2d(x, self.W1, stride=self.strd1) + self.b1)
        gamma2 = F.relu(self.c2*F.conv2d(gamma1, self.W2, stride=self.strd2) + self.b2) 
        gamma3 = F.relu(self.c3*F.conv2d(gamma2, self.W3, stride=self.strd3) + self.b3) 
        
        for _ in  range(self.T):
            
            # backward computation
            gamma2 = F.conv_transpose2d(gamma3,self.W3, stride=self.strd3)
            gamma1 = F.conv_transpose2d(gamma2,self.W2, stride=self.strd2)

            # forward computation
            gamma1 = F.relu( (gamma1 - self.c1*F.conv2d( F.conv_transpose2d(gamma1,self.W1, stride = self.strd1) - x ,self.W1, stride = self.strd1)) + self.b1)
            gamma2 = F.relu( (gamma2 - self.c2*F.conv2d( F.conv_transpose2d(gamma2,self.W2, stride = self.strd2) - gamma1, self.W2, stride = self.strd2)) + self.b2) 
            gamma3 = F.relu( (gamma3 - self.c3*F.conv2d( F.conv_transpose2d(gamma3,self.W3, stride = self.strd3) - gamma2, self.W3, stride = self.strd3)) + self.b3) 

        # invertibility?
        gamma2 = gamma2 - F.conv_transpose2d(gamma3,self.W3, stride=self.strd3)
        gamma2 = F.relu(gamma2)
        gamma1 = gamma1 - F.conv_transpose2d(gamma2,self.W2, stride=self.strd2)
        gamma1 = F.relu(gamma1)
        x_Rec = x - F.conv_transpose2d(gamma1,self.W1, stride = self.strd1)
        x_Rec = F.relu(x_Rec)
            
        return x_Rec.detach()


def train_model_2(param):
    #  use gpu if available
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # create a model from `ML_ISTA_NET_HAE` autoencoder class
    # load it to gpu
    model = ML_ISTA_NET_HAE(param["m1"], param["m2"], param["m3"], param["Unfoldings"])
    model = model.cuda()
    # create an optimizer object
    # SGD optimizer
    optimizer_ae = torch.optim.SGD(model.parameters(), lr=param["LR"], weight_decay=param["WD"])

    # mean-squared error loss
    criterion = nn.MSELoss()

    train_dataset = torchvision.datasets.MNIST(
        root="../data", 
        train=True, 
        transform=torchvision.transforms.ToTensor(), 
        download=False
    )

    test_dataset = torchvision.datasets.MNIST(
        root="../data", 
        train=False, 
        transform=torchvision.transforms.ToTensor(), 
        download=False
    )

    train_loader = torch.utils.data.DataLoader(
        train_dataset, batch_size=128, shuffle=True
    )

    test_loader = torch.utils.data.DataLoader(
        test_dataset, batch_size=128, shuffle=True
    )
    
    model.train()
    
    for epoch in range(param["epoch"]):
        pred = 0
        correct = 0
        loss = 0
        for batch_features, _ in train_loader:
            
            # reset the gradients back to zero
            # PyTorch accumulates gradients on subsequent backward passes
            optimizer_ae.zero_grad()

            # compute reconstructions
            outputs = model(batch_features.cuda()) # fetch output image
            outputs = Variable(outputs.cuda(), requires_grad=True) # transform output to variable so we can use requires_grad 
            
            # compute training reconstruction loss
            batch_features = Variable(batch_features.cuda(), requires_grad=True)
            train_loss = criterion(outputs, batch_features.cuda())
            
            # compute accumulated gradients
            train_loss.backward() 

            # perform parameter update based on current gradients
            optimizer_ae.step()

            # add the mini-batch training loss to epoch loss
            loss += train_loss.item()
        print("epoch : {}/{}, loss = {:.6f}".format(epoch + 1, param["epoch"], loss / len(train_loader)))
        
    # compute the epoch training loss
    loss = loss / len(train_loader)

    # display the epoch training loss
    print("epoch : {}/{}, loss = {:.6f}".format(epoch + 1, param["epoch"], loss))
    
    
    
    
    # testing
    model.eval()
    reconstruction = []
    test_examples = None
    with torch.no_grad():
        for batch_features, _ in test_loader:
            reconstruction.append(model(batch_features.cuda()))
            test_examples = batch_features
            break
        
    # visualization
    plt.figure(figsize=(20, 4))
    with torch.no_grad():
        for i in range(10):
            # display original
            ax = plt.subplot(2, 10, i + 1)
            plt.imshow(test_examples[i][0].numpy().reshape(28, 28, 1))
            plt.gray()
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)

            # display reconstruction
            ax = plt.subplot(2, 10, i + 1 + 10)
            plt.imshow(reconstruction[0][i].cpu().numpy().reshape(28, 28, 1))
            plt.gray()
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)
            number = math.floor(time.time())
        plt.savefig("AE_MNIST_res_" + str(number), bbox_inches='tight')
    print("Saved figure AE_MNIST_res_" + str(number))

if __name__ == "__main__":
    param = {"m1": 64, "m2": 32, "m3": 16, "m4": 8, "m5": 32, "m6": 64, "m7": 128,"LR": 0.01, "WD": 1e-5, "MN": 0.9, "gamma_sched": 0.2, "step_size_sch": 30, "Unfoldings": 6, "BS": 128, "epoch": 20, "mnist": False}
    train_model_2(param)