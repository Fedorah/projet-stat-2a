import torch
# import torch.nn as nn
from torch.autograd import Variable
import torch.utils.data as Data
import torch.nn.functional as F
import torchvision
# import torchvision.transforms as transforms
import matplotlib.pyplot as plt
# from matplotlib import cm
import numpy as np
import time
import progressbar
import Models as models
# import pdb for debugging
# import importlib

plt.style.use('dark_background')
# torch.manual_seed(1)    # reproducible


def setUp():
    # torch.cuda.set_device(1)

    # Hyper Parameters
    global EPOCH
    global BATCH_SIZE
    global cudaopt
    EPOCH = 10
    BATCH_SIZE = 128
    DOWNLOAD_MNIST = True
    cudaopt = True

    # Network parameteres: filters per layer. More definitions in the Models.py
    m1 = 32
    m2 = 4
    m3 = 32

    # Training parameters
    LR = 0.02
    WD = 1e-5
    MN = .9
    gamma_sched = .2
    step_size_sch = 30
    global Unfoldings
    Unfoldings = 6

    param = {"m1": m1, "m2": m2, "m3": m3, "LR": LR, "WD": WD, "MN": MN, "gamma_sched": gamma_sched, "step_size_sch": step_size_sch, "Unfoldings": Unfoldings, "BS": BATCH_SIZE, "epoch": EPOCH}


    # Mnist digits dataset
    train_data = torchvision.datasets.MNIST(
        root='../data',
        train=True,                                     # this is training data
        transform=torchvision.transforms.ToTensor(),    # Converts a PIL.Image or numpy.ndarray to
                                                        # torch.FloatTensor of shape (C x H x W) and normalize in the range [0.0, 1.0]
        download=DOWNLOAD_MNIST                         # download it if you don't have it
    )
 
    # TRAINING DATA
    Ntrain = int(60e3)
    param["Ntrain"] = Ntrain
    train_set = np.random.permutation(60000)[0:param["Ntrain"]]
    train_data.data = train_data.data[torch.LongTensor(train_set), :, :]
    train_data.targets = train_data.targets[torch.LongTensor(train_set)]

    test_data = torchvision.datasets.MNIST(
        root='../data',
        train=False,                                     # this is testing data
        transform=torchvision.transforms.ToTensor(),    # Converts a PIL.Image or numpy.ndarray to
                                                        # torch.FloatTensor of shape (C x H x W) and normalize in the range [0.0, 1.0]
        download=DOWNLOAD_MNIST,                        # download it if you don't have it
    )

    train_loader = Data.DataLoader(dataset=train_data, batch_size=param["BS"], shuffle=True)
    test_loader = Data.DataLoader(dataset=test_data, batch_size=param["BS"], shuffle=True)
    
    model_mlista = models.ML_ISTA_NET(param["m1"], param["m2"], param["m3"], param["Unfoldings"])
    optimizer = torch.optim.SGD(model_mlista.parameters(), lr=param["LR"], momentum=param["MN"], weight_decay=param["WD"])
    scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=param["step_size_sch"], gamma=param["gamma_sched"])
    model_mlista, Acc_train_mlista, Acc_test_mlista, intermediate_mlista = train_model(model_mlista.cuda(), train_loader, test_loader, optimizer, scheduler, param["epoch"])
    
    model_mlfista = models.ML_FISTA_NET(param["m1"], param["m2"], param["m3"], param["Unfoldings"])
    optimizer = torch.optim.SGD(model_mlfista.parameters(), lr=param["LR"], momentum=param["MN"], weight_decay=param["WD"])
    scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=param["step_size_sch"], gamma=param["gamma_sched"])
    model_mlfista, Acc_train_mlfista, Acc_test_mlfista, intermediate_mlfista = train_model(model_mlfista.cuda(), train_loader, test_loader, optimizer, scheduler, param["epoch"])
    
    plt.figure(figsize=(3, 3))
    plt.style.use('default')
    plt.plot(Acc_test_mlista, label='ML-ISTA')
    plt.plot(Acc_test_mlfista, label='ML-FISTA')

    plt.title('Test Accuracy - MNIST')
    plt.grid()
    plt.axis([0, EPOCH, 97, 99.5])
    plt.xlabel('Epoch')
    plt.legend()
    plt.savefig("MNIST_results.pdf", bbox_inches='tight')
    
    print('. ML-ISTA: ', np.mean(Acc_test_mlista[-20:-1]), '. ML-FISTA: ', np.mean(Acc_test_mlfista[-20:-1])) 
    

def train_model(model, train_loader, test_loader, optimizer, scheduler,EPOCH):
    t0 = time.perf_counter()
    tprev = t0

    Loss_train = np.zeros((EPOCH,))
    Loss_test = np.zeros((EPOCH,))
    Acc_test = np.zeros((EPOCH,))
    Acc_train = np.zeros((EPOCH,))
    Time_test = np.zeros((EPOCH,))

    bar = progressbar.ProgressBar(EPOCH-1)
    bar.start()

    for epoch in range(EPOCH):
        scheduler.step()
        bar.update(epoch)
        model.train()

        correct = 0
        train_loss = 0
        rec_error = 0
        intermediate_sets = []
        for step, (x, y) in enumerate(train_loader):
            b_x = Variable(x) 
            b_y = Variable(y)               
            if cudaopt:
                b_y, b_x = b_y.cuda(), b_x.cuda()
            scores = model(b_x)
            loss = F.nll_loss(scores, b_y)      # negative log likelyhood
            optimizer.zero_grad()               # clear gradients for this training step
            loss.backward()                     # backpropagation, compute gradients
            optimizer.step()                    # apply gradients
            intermediate_sets.append(optimizer.state_dict())
            model.zero_grad()
            
            # computing training stats
            pred = scores.data.max(1, keepdim=True)[1]
            correct += pred.eq(b_y.data.view_as(pred)).long().cpu().sum()
            train_loss += F.nll_loss(scores, b_y, reduction='sum').data.item()
            
        Acc_train[epoch] = 100 * float(correct) / float(len(train_loader.dataset))
        Loss_train[epoch] = train_loss / len(train_loader.dataset)  # Defining training functionn(train_loader.dataset)

        # testing
        model.eval()
        correct = 0
        test_loss = 0
        NNZ = 0.0
        for step, (x, y) in enumerate(test_loader):
            b_x = Variable(x)   
            b_y = Variable(y)               # batch label
            if cudaopt:
                b_y, b_x = b_y.cuda(), b_x.cuda()
            scores = model(b_x)
            test_loss += F.nll_loss(scores, b_y, reduction='sum').data.item()
            pred = scores.data.max(1, keepdim=True)[1]
            correct += pred.eq(b_y.data.view_as(pred)).long().cpu().sum()

        Loss_test[epoch] = test_loss/len(test_loader.dataset)
        Acc_test[epoch] = 100 * float(correct) / float(len(test_loader.dataset))
        Time_test[epoch] = time.perf_counter()-t0
        
         # print('Epoch: ', epoch, '| train loss: ', train_loss.data.cpu().numpy()/len(train_loader.dataset),'| train acc: ', Acc_train[epoch], '%','| test acc: ', Acc_test[epoch], '%')
    
    return model, Acc_train, Acc_test, intermediate_sets


def compare(intermediate, input):
    size = np.size(intermediate)
    diff = 0
    for i in range(size):
        for j in range(size):
            diff += abs(intermediate[i, j] - input[i, j])
    return diff

def optimizeParameters(diff, input, parameters):
    pass


def main():
    setUp()


if __name__ == "__main__":
    main()

