# Multi Layer ISTA and FISTA for CNNs

Demo for Multi-Layer ISTA and Multi-Layer FISTA algorithms for convolutional neural networks, as described in [J. Sulam, A. Aberdam, A. Beck, M. Elad, (2018). On Multi-Layer Basis Pursuit, Efficient Algorithms and Convolutional Neural Networks. arXiv preprint:1806.00701](https://arxiv.org/abs/1806.00701)

# Trying to make an autoencoder with pytorch using the model developped in the work cited above

### **Requirements** :

The file requirements.txt does not work, please ignore it.

Use the following to download the required packages instead:

```
pip install torch
pip install torchvision
pip intall numpy
pip install progressbar
pip install matplotlib
```

### **MNIST**

```
cd MNIST
python autoencoder_hybride.py
```

default parameters used:

```
param = {"m1": 64, "m2": 32, "m3": 16, "LR": 0.001, "WD": 1e-5, "MN": 0.9, "gamma_sched": 0.2, "step_size_sch": 30, "Unfoldings": 6, "BS": 128, "epoch": 20, "mnist": False}
```

if you don't have the MNIST dataset you can download it by changing

```
param["mnist"]
```

to

```
True
```

Only the autoencoder.py file is needed.

### **CIFAR**

To try the hybrid autoencoder with CIFAR, do :
```
cd CIFAR
python autoencoder_hybride.py
```

default parameters used:

```
param = {"m1": 64, "m2": 32, "m3": 16, "LR": 0.001, "WD": 1e-5, "MN": 0.9, "gamma_sched": 0.2, "step_size_sch": 30, "Unfoldings": 6, "BS": 128, "epoch": 20, "cifar": False}
```

if you don't have the CIFAR dataset you can download it by changing

```
param["cifar"]
```

to

```
True
```
