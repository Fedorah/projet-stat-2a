import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.utils.data as Data
import torch.nn.functional as F
import torchvision
import torchvision.transforms as transforms
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
import math
import time
import pdb


class ML_ISTA_NET(nn.Module):
    def __init__(self,m1,m2,m3,T):
        super(ML_ISTA_NET, self).__init__()
        
        self.T = T
        
        # Convolutional Filters
        self.W1 = nn.Parameter(torch.randn(m1, 3, 6, 6), requires_grad=True)
        self.strd1 = 2;
        self.W2 = nn.Parameter(torch.randn(m2, m1, 6, 6), requires_grad=True)
        self.strd2 = 2;
        self.W3 = nn.Parameter(torch.randn(m3, m2, 4, 4), requires_grad=True)
        self.strd3 = 1;
        
        self.c1 = nn.Parameter(torch.ones(1, 1, 1, 1), requires_grad=True)
        self.c2 = nn.Parameter(torch.ones(1, 1, 1, 1), requires_grad=True)
        self.c3 = nn.Parameter(torch.ones(1, 1, 1, 1), requires_grad=True)
        
        # Biases / Thresholds
        self.b1 = nn.Parameter(torch.zeros(1, m1, 1, 1), requires_grad=True)
        self.b2 = nn.Parameter(torch.zeros(1, m2, 1, 1), requires_grad=True)
        self.b3 = nn.Parameter(torch.zeros(1, m3, 1, 1), requires_grad=True)
        
        # Initialization
        self.W1.data = .1 /np.sqrt(36) * self.W1.data
        self.W2.data = .1 /np.sqrt(m1*36) * self.W2.data
        self.W3.data = .1 /np.sqrt(m2*16) * self.W3.data
        
    def forward(self, x):
        # Encoding
        gamma1 = F.relu(self.c1*F.conv2d(x, self.W1, stride=self.strd1) + self.b1)
        gamma2 = F.relu(self.c2*F.conv2d(gamma1, self.W2, stride=self.strd2) + self.b2) 
        gamma3 = F.relu(self.c3*F.conv2d(gamma2, self.W3, stride=self.strd3) + self.b3) 
        
        for _ in  range(self.T):
            
            # backward computation
            gamma2 = F.conv_transpose2d(gamma3,self.W3, stride=self.strd3)
            gamma1 = F.conv_transpose2d(gamma2,self.W2, stride=self.strd2)

            # forward computation
            gamma1 = F.relu( (gamma1 - self.c1*F.conv2d( F.conv_transpose2d(gamma1,self.W1, stride = self.strd1) - x ,self.W1, stride = self.strd1)) + self.b1)
            gamma2 = F.relu( (gamma2 - self.c2*F.conv2d( F.conv_transpose2d(gamma2,self.W2, stride = self.strd2) - gamma1, self.W2, stride = self.strd2)) + self.b2) 
            gamma3 = F.relu( (gamma3 - self.c3*F.conv2d( F.conv_transpose2d(gamma3,self.W3, stride = self.strd3) - gamma2, self.W3, stride = self.strd3)) + self.b3) 

        # invertibility?
        gamma2 = gamma2 - F.conv_transpose2d(gamma3, self.W3, stride=self.strd3)
        gamma2 = F.relu(gamma2)
        gamma1 = gamma1 - F.conv_transpose2d(gamma2, self.W2, stride=self.strd2)
        gamma1 = F.relu(gamma1)
        x_Rec = x - F.conv_transpose2d(gamma1, self.W1, stride=self.strd1)
        x_Rec = F.relu(x_Rec)
        x_Rec = F.adaptive_max_pool2d(x_Rec, output_size=(32, 32))
        x_Rec = F.adaptive_max_pool2d(x_Rec, output_size=(32, 32))
            
        return x_Rec.detach()


#Utility functions to un-normalize and display an image
def imshow(img):
    img = img / 2 + 0.5
    plt.imshow(np.transpose(img))
    
def train_model_2(param):
    #  use gpu if available
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # create a model from `AE` autoencoder class
    # load it to gpu
    model = ML_ISTA_NET(param["m1"], param["m2"], param["m3"], param["Unfoldings"])
    model = model.cuda()
    # create an optimizer object
    # SGD optimizer
    optimizer_ae = torch.optim.SGD(model.parameters(), lr=param["LR"], weight_decay=param["WD"])

    # mean-squared error loss
    criterion = nn.MSELoss()
    
    print('==> Preparing data..')
    transform_train = transforms.Compose([
        transforms.RandomCrop(32, padding=4),
        transforms.ToTensor(),
        transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
    ])
    transform_test = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
    ])
    
    train_dataset = torchvision.datasets.CIFAR10(root='../data/CIFAR', train=True, download=param["cifar"], transform=transform_train)

    test_dataset = torchvision.datasets.CIFAR10(root='../data/CIFAR', train=False, download=param["cifar"], transform=transform_test)

    train_loader = Data.DataLoader(dataset=train_dataset, batch_size=param["BS"], shuffle=True)

    test_loader = Data.DataLoader(dataset=test_dataset, batch_size=param["BS"], shuffle=True)
    
    model.train()
    
    for epoch in range(param["epoch"]):
        pred = 0
        correct = 0
        loss = 0
        for batch_features, _ in train_loader:
            
            # reset the gradients back to zero
            # PyTorch accumulates gradients on subsequent backward passes
            optimizer_ae.zero_grad()

            # compute reconstructions
            outputs = model(batch_features.cuda()) # fetch output image
            outputs = Variable(outputs.cuda(), requires_grad=True) # transform output to variable so we can use requires_grad 
            
            # compute training reconstruction loss
            batch_features = Variable(batch_features.cuda(), requires_grad=True)
            train_loss = criterion(outputs, batch_features.cuda())
            
            # compute accumulated gradients
            train_loss.backward() 

            # perform parameter update based on current gradients
            optimizer_ae.step()

            # add the mini-batch training loss to epoch loss
            loss += train_loss.item()
        print("epoch : {}/{}, loss = {:.6f}".format(epoch + 1, param["epoch"], loss / len(train_loader)))
        
    # compute the epoch training loss
    loss = loss / len(train_loader)

    # display the epoch training loss
    print("epoch : {}/{}, loss = {:.6f}".format(epoch + 1, param["epoch"], loss))
        
    # visualization
    
    model.eval()
    reconstruction = []
    test_examples = None
    with torch.no_grad():
        for batch_features, _ in test_loader:
            reconstruction.append(model(batch_features.cuda()))
            test_examples = batch_features
            break

    test_examples = test_examples.view(param["BS"], 3, 32, 32)
    test_examples = test_examples.detach().cpu().numpy()
    
    plt.figure(figsize=(20, 4))
    with torch.no_grad():
        for i in range(10):
            # display original
            ax = plt.subplot(2, 10, i + 1)
            plt.imshow(test_examples[i][0].reshape(32, 32, 1))
            plt.gray()
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)

            # display reconstruction
            ax = plt.subplot(2, 10, i + 1 + 10)
            plt.imshow(reconstruction[0][i].cpu().numpy().reshape(32, 32, 3))
            plt.gray()
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)
            number = math.floor(time.time())
        plt.savefig("HAE_CIFAR_res_" + str(number), bbox_inches='tight')
    print("Saved figure HAE_CIFAR_res_" + str(number))
    
    
    """
    # visualization
    model.eval()
    reconstruction = []
    test_examples = None
    with torch.no_grad():
        for batch_features, _ in test_loader:
            reconstruction.append(model(batch_features.cuda()))
            test_examples = batch_features
            break

    test_examples = test_examples.view(param["BS"], 3, 32, 32)
    test_examples = test_examples.detach().cpu().numpy()

    # Original Images
    print("Original Images")
    fig, axes = plt.subplots(nrows=1, ncols=5, sharex=True, sharey=True, figsize=(20,4))
    for idx in np.arange(5):
        ax = fig.add_subplot(2, 10, idx + 1 + 10, xticks=[], yticks=[])
        imshow(test_examples[0][idx])
        # ax.set_title(classes[labels[idx]])
        ax = fig.add_subplot(2, 10, idx + 1 + 10, xticks=[], yticks=[])
        imshow(reconstruction[0][idx].cpu())
        # ax.set_title(classes[labels[idx]])
    number = math.floor(time.time())
    plt.savefig("HAE_CIFAR_res_" + str(number), bbox_inches='tight')
    print("Saved figure HAE_CIFAR_res_" + str(number))"""

    
if __name__ == "__main__":
    param = {"m1": 64, "m2": 32, "m3": 16, "m4": 8, "m5": 32, "m6": 64, "m7": 128,"LR": 0.01, "WD": 1e-5, "MN": 0.9, "gamma_sched": 0.2, "step_size_sch": 30, "Unfoldings": 6, "BS": 128, "epoch": 20, "cifar": False}
    train_model_2(param)